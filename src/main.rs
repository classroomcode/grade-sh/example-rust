use std::io::Write;
mod convert_to_binary;

fn main() {
    // Purpose:    Drives convert to binary.
    // Parameters: None
    // User Input: If no args, input dec number to convert
    // Prints:     If no args, then print result
    // Returns:    Nothing
    // Modifies:   Nothing
    // Calls:      std:: stuff
    // Tests:      arg_tests/ and stdio_tests/
    // Status:     Done!
    let args: Vec<String> = std::env::args().collect();
    if args.len() == 3 {
        let infile = &args[1];
        let outfile = &args[2];
        let num_to_convert =
            std::fs::read_to_string(infile).expect("Something went wrong reading the file");
        let num_to_convert: u64 = num_to_convert
            .trim()
            .parse()
            .expect("Please type a number!");
        let mut outfile = std::fs::File::create(outfile).expect("create failed");
        let mut converted = convert_to_binary::convert_to_binary(num_to_convert);
        converted.push_str("\n");
        outfile
            .write_all(converted.as_bytes())
            .expect("write failed");
    } else {
        let mut num_to_convert = String::new();
        std::io::stdin()
            .read_line(&mut num_to_convert)
            .expect("Failed to read line");
        let num_to_convert: u64 = num_to_convert
            .trim()
            .parse()
            .expect("Please type a number!");
        println!("{}", convert_to_binary::convert_to_binary(num_to_convert));
    }
}
