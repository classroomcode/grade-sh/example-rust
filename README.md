# Programming assignment 00
A template assignment: Convert to Binary

## Problem description
Write a function that converts decimal to binary, stripping leading 0s.
Write a main driver that uses this function, accepts one command line argument, 
and outputs the binary conversion to std-out.

### Inputs
One decimal number.

### Outputs
One binary number, with leading 0s striped.

### Code specifications
In general, most functions should not accept input or produce output via std-io. 
Instead, leave that job for main.

## Git autograding 
Git clone your repository onto your machine, and run the autograder.
See: [docs/git_autograding.md](docs/git_autograding.md)
