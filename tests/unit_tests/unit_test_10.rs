/* TODO This unit test needs:
 * exception handling
 * a decorator
 */
#[path = "../../src/convert_to_binary.rs"]
mod convert_to_binary;

fn main() {
    let args: Vec<String> = std::env::args().collect();
    if args.len() < 2 {
        println!("Must include returncode as arg")
    }
    let desired_retcode = &args[1];
    let desired_retcode: i32 = desired_retcode
        .trim()
        .parse()
        .expect("Please type a number!");
    if convert_to_binary::convert_to_binary(10) == "1010" {
        std::process::exit(desired_retcode);
    } else {
        std::process::exit(1);
    }
}
